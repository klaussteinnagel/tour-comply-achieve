---
title: Citation Wizard
---
# Citation Wizard

The Citation Wizard is an optional ComplyAchieve component.  If your subscription includes the Citation Wizard, you'll be able to build your own customized citation lists using a simple applicability questionnaire approach. You select the jurisdictions and topics that apply to you and then answer a series of survey questions; behind the scenes, the Citation Wizard analyzes your responses and generates a list of citations that are applicable to your company at the state and federal level. On the **Build** tab, open the **Citation Wizard** and select the jurisdiction(s) and topic(s) that apply to your company. After answering a series of Applicability Analysis survey questions, the Citation Wizard will determine whether each regulatory area is _Applicable_ or _Not Applicable_ to your company, generating a list of citations for each _Applicable_ area. To add the generated citations to the **Approve** tab for research and review, click the **Add to Approve** button.

![wizard](/achieve/images/wizard.jpg "wizard")
