---
title: Welcome
---
# Welcome

Bloomberg BNA’s new **Comply Achieve** tool helps you build, approve and monitor regulations that affect your organization.

* Identify and build a list of regulations that affect your company.
* Analyze and approve citations from the list you built
* Monitor your approved citation list

To begin, **Build** a list of the citations you wish to track.

![welcome](/achieve/images/welcome.jpg "welcome")
